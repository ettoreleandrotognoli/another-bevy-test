use bevy::prelude::*;
use bevy_rapier3d::prelude::*;
use bevy_asset_loader::prelude::*;
use leafwing_input_manager::prelude::*;
use bevy_inspector_egui::WorldInspectorPlugin;

use crate::player::camera::FixedCamera;
use crate::player::{Action, Player, PlayerPlugin};

mod player;


#[derive(AssetCollection)]
struct MyAssets {
    #[asset(path = "images/player.png")]
    player: Handle<Image>,
    #[asset(path = "walking.ogg")]
    walking: Handle<AudioSource>,
}

#[derive(Clone, Eq, PartialEq, Debug, Hash)]
enum GameState {
    AssetLoading,
    Next,
}

fn use_my_assets(_my_assets: Res<MyAssets>) {
    // do something using the asset handles from the resource
}


fn main() {
    App::new()
        .add_loading_state(
            LoadingState::new(GameState::AssetLoading)
                .continue_to_state(GameState::Next)
                .with_collection::<MyAssets>()
        )
        .add_state(GameState::AssetLoading)
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<NoUserData>::default())
        .add_plugin(RapierDebugRenderPlugin::default())
        .add_plugin(WorldInspectorPlugin::new())
        .add_plugin(InputManagerPlugin::<Action>::default())
        .add_system_set(SystemSet::on_enter(GameState::Next).with_system(use_my_assets))
        .add_plugin(PlayerPlugin {})
        .add_startup_system(startup)
        .run();
}

fn startup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands.insert_resource(AmbientLight {
        color: Color::WHITE,
        brightness: 1.,
    });
    let camera = FixedCamera::spawn(&mut commands, Vec3::new(0., 2.5, -5.));
    commands.entity(camera)
        .insert(RigidBody::Dynamic)
        .insert(Velocity {
            linvel: Vec3::new(0.0, 0.0, -0.5),
            angvel: Vec3::new(0.0, 0.0, 0.0),
        });

    Player::spawn(&mut commands, &mut meshes, &mut materials);
    commands.spawn_bundle(PbrBundle {
        transform: Transform::from_xyz(0., 0., 0.),
        mesh: meshes.add(Mesh::from(shape::Plane { size: 10. })),
        material: materials.add(Color::rgb(0.5, 0.5, 0.5).into()),
        ..Default::default()
    });

    // commands.spawn_bundle(PbrBundle {
    //     transform: Transform::from_xyz(0., 2.5/2., 0.),
    //     mesh: meshes.add(Mesh::from(shape::Cube { size: 2.5 })),
    //     material: materials.add(Color::rgb(0.9, 0.3, 0.3).into()),
    //     ..Default::default()
    // });
}