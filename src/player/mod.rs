use bevy::prelude::*;
use leafwing_input_manager::prelude::*;

use crate::player::camera::CameraPlugin;

pub mod camera;

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
pub struct Player {}


impl Player {
    pub fn spawn(
        command: &mut Commands,
        meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) -> Entity {
        command.spawn_bundle(PbrBundle {
            transform: Transform::from_translation(Vec3::ZERO),
            mesh: meshes.add(shape::Capsule::default().into()),
            material: materials.add(Color::RED.into()),
            ..default()
        })
            .insert(Name::new("Player"))
            .insert(Player {})
            .id()
    }
}

#[derive(Actionlike, PartialEq, Eq, Clone, Copy, Hash, Debug)]
pub enum Action {
    Run,
    Jump,
}

pub struct PlayerPlugin {}

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app
            .register_type::<Player>()
            .add_plugin(CameraPlugin {});
    }
}