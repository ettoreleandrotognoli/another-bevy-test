use bevy::ecs::system::EntityCommands;
use bevy::prelude::*;

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
pub struct FirstPersonCamera {}

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
pub struct ThirdPersonCamera {
    offset: Vec3,
}

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
pub struct FixedCamera {}

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
pub struct FlyingCamera {}


impl FixedCamera {
    pub fn spawn(commands: &mut Commands, position: Vec3) -> Entity {
        commands
            .spawn_bundle(Camera3dBundle {
                transform: Transform::from_translation(position).looking_at(Vec3::ZERO, Vec3::Y),
                ..Default::default()
            })
            .insert(FixedCamera {})
            .id()
    }
}

pub fn spawn_third_person_camera(
    mut commands: Commands
) -> Entity {
    commands
        .spawn_bundle(Camera3dBundle {
            ..Default::default()
        })
        .id()
}

pub struct CameraPlugin {}

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app
            .register_type::<FlyingCamera>()
            .register_type::<FirstPersonCamera>()
            .register_type::<ThirdPersonCamera>()
            .register_type::<FixedCamera>();
    }
}